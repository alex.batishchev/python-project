MY SUPER APP
------------------

## Local Installation

It's easy to install and run it on your computer.

```shell
# 1. First, clone the repo
$ git clone
$ cd keras-flask-deploy-webapp

# 2. Install Python packages
$ pip install -r requirements.txt

# 3. Run!
$ python app.py
```

Open http://localhost:5000 and have fun. :smiley:
